public class App {
    public static void main(String[] args) throws Exception {
        calcular();
    }

    public static void calcular(){
        int numero = 5;
        int resultado = (numero%2 == 0) ? numero*2 : numero*3;
        System.out.println(resultado);
    }

    public static void estructuras(){
        int var_1 = 10;
        int var_2 = 20;
        //condicionales
        if(var_1 < var_2){
            //instrucciones
            System.out.println("es verdad");
        }else if(var_2 < var_1){
            System.out.println("es falso");
        }
        
        System.out.println("--------WHILE----------");
        int cont = 0;
        while(cont < 5){
            System.out.println(cont);
            ++cont;
        }
        cont = 0;
        System.out.println("--------DO-WHILE----------");
        do{
            System.out.println(cont);
            cont += 1;
        }while(cont < 5);

        System.out.println("--------FOR----------");
        for(int i = 0; i < 10; i++){
            System.out.println(i);
        }

        int contador = 0;
        ++contador;
        contador++;
    }

    //Método
    public static void saludar(){
        System.out.println("Hola mundo");
    }

}
